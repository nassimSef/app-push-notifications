$(document).ready(function(){
    particlesJS.load('particles-js', '../../dist/js/particlesjs-config.json', function(e) {
        //console.log('callback - particles.js config loaded');
    });

    // Menu Animation
    var content = document.querySelector('.hamburger');
    $('.hamburger').on('click', function(e){
        if(content.classList.contains('is-active') !== true){
            $('.hamburger').addClass('is-active')
            gsap.fromTo("#bg-mobile", {y: -1000}, {y: -84, duration: 1, ease: "power.out"});
        }else {
            $('.hamburger').removeClass('is-active')
            gsap.fromTo("#bg-mobile", {y: -84}, {y: -1000, duration: 1, ease: "power.out"});
        }
        e.preventDefault();
    })

    // Date Plugin
    $('[data-toggle="datepicker"]').datepicker({
        format: 'yyyy-mm-dd'
    });

    // PopUp Event
    var hasClass = document.querySelector('.popupAdd');
    $('.addBtn').on('click', function(e) {
        console.log('click')
        if (hasClass.classList.contains('show') !== true) {
            $('.popupAdd').addClass('show');
        }else{
            $('.popupAdd').removeClass('show')
        }
        $('.close').on('click', function(e) {
            $('.popupAdd').removeClass('show')
        })
    })

    // Results Events
    $('.menu-profile').on('click', function(){
        $(this).addClass('active');
        $(this).children().addClass('active');
        $('.menu-bons').removeClass('active');
        $('.menu-bons').children().removeClass('active');
        $('.menu-achat').removeClass('active');
        $('.menu-achat').children().removeClass('active');
        $('.profile').removeClass('hide');
        $('.info-card').addClass('hide');
        $('.testing').addClass('hide');
    })
    $('.menu-bons').on('click', function(){
        $(this).addClass('active');
        $(this).children().addClass('active');
        $('.menu-profile').removeClass('active');
        $('.menu-profile').children().removeClass('active');
        $('.menu-achat').removeClass('active');
        $('.menu-achat').children().removeClass('active');
        $('.profile').addClass('hide');
        $('.info-card').removeClass('hide');
        $('.testing').addClass('hide');
    })
    $('.menu-achat').on('click', function(){
        $(this).addClass('active');
        $(this).children().addClass('active');
        $('.menu-bons').removeClass('active');
        $('.menu-bons').children().removeClass('active');
        $('.menu-profile').removeClass('active');
        $('.menu-profile').children().removeClass('active');
        $('.profile').addClass('hide');
        $('.info-card').addClass('hide');
        $('.testing').removeClass('hide');
    })
})